﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MyBinaryTree//Класс узлов дерева
{
    class Node
    {
        public int key;//ключ узла
        public Node left; //левый потомок
        public Node right;//правый потомок
        public Node(int x)////Конструктор с параметром, передаем значение х
        {
            key = x;//При вызове конструктора с параметром, ключ узла будет иметь значение x
            left = null;//левый дочерний узел равен null
            right = null;//правый дочерний узел равен null
        }
    }   
    
    class AVLtree//Класс AVL-дерева
    {
        public Node root;//Корень дерева
        public AVLtree()//Конструктор по умолчанию
        {
            root = null;//По умолчанию создаётся дерево с корнем, имеющим значение null
        }       

        private int max(int a, int b)//Функция, возвращающая максимальное из двух чисел
        {
            return (a > b) ? a : b;//Если a>b, то вернёт a, иначе вернёт b
        }

        private Node RotateLeft(Node parent)//левый поворот вокруг узла parent
        {
            Node pivot = parent.right; //копируем правый дочерний узел
            parent.right = pivot.left; //правому дочернему узлу присваем значение левого дочернего от копии
            pivot.left = parent; //левый дочерний узел копии становится узлом, относительно которого делали поворот
            return pivot;//возвращаем новый корень поддерева
        }

        private Node RotateRight(Node parent)//правый поворот вокруг узла parent(parent-опорный узел)
        {
            Node pivot = parent.left;
            parent.left = pivot.right;
            pivot.right = parent; 
            return pivot;
        }

        private Node BigRotateRight(Node parent)//большой правый поворот вокруг узла parent
        {
            Node pivot = parent.left;//делаем копию левого потомка
            parent.left = RotateLeft(pivot);//сначала делается левый поворот вокруг левого потомка
            return RotateRight(parent);//потом правый поворот вокруг нужного узла
        }

        private Node BigRotateLeft(Node parent)//большой левый поворот вокруг узла parent
        {
            Node pivot = parent.right;//делаем копию првого потомка
            parent.right = RotateRight (pivot);//сначала делается правый поворот вокруг правого правого потомка
            return RotateLeft(parent);//потом левый поворот вокруг нужного узла
        }

        public void insert(int data)//Функция добавления узла в дерево со значением data
        {
            Node newItem = new Node(data);//создается новый узел
            if (root == null)//если добавляем в пуcтое дерево,
            {
                root = newItem;//то добавленный узел становится его корнем
            }
            else
            {
                root = RecursiveInsert(root, newItem);//иначе запускаем рекурсию
            }
        }

        private Node RecursiveInsert(Node current, Node n)//добавляем узел в непустое поддерево с корнем current
         {
            if (current == null)//если текущий корень дерева равен null
            {
                current = n; //то добавленный узел становится новым корнем поддерева
                return current;//Добавили,и вышли из рекурсии, возвращаем новый корень поддерева
            }
            else if (n.key < current.key)//иначе если добавляем узел со значением меньше чем у корня поддерева, то
            {
                current.left = RecursiveInsert(current.left, n); //запускаем рекурсию влево
                current = balance_tree(current); //балансируем
            }
            else if (n.key > current.key)//иначе если добавляем узел со значением больше чем у корня поддерева, то
            {
                current.right = RecursiveInsert(current.right, n);//запускаем рекурсию вправо 
                current = balance_tree(current);//балансируем
            }
            return current;//возвращаем новый корень поддерева
        }

        private int getHeight(Node current)//Функция, вычисляющая высоту узла
        {
            int height = 0;//изначально высота 0
            if (current != null)//пока не упремся в краевой узел
            {
                int l = getHeight(current.left);//рекурсивно спускаемся влево
                int r = getHeight(current.right);//рекурсивно спускаемся вправо
                int m = max(l, r);//сравниваем какой из дочерних узлов выше
                height = m + 1;//инкрементируем
            }
            return height;//возвращаем высоту
        }

        private int balance_factor(Node current)//функция, вычисляющая для узла n разность высот левого и правого дочерних узлов
        {
            int l = getHeight(current.left);//получили высоту левого дочернего узла
            int r = getHeight(current.right);//получили высоту правого дочернего узла
            int b_factor = l - r;//вычислили разность
            return b_factor;//возвращаем разность
        }

        private Node balance_tree(Node current)//балансировка дерева
        {
            int b_factor = balance_factor(current);//Проверям разницу высот левого и правоого дочерних узлов
            if (b_factor > 1)//если левый дочерний узел выше правого больше чем на 1
            {
                if (balance_factor(current.left) > 0)//если у этого дочернего узла левый дочерний узел выше правого
                {
                    current = RotateRight(current);//то совершаем правый поворот
                }
                else//иначе
                {
                    current = BigRotateRight(current);//совершаем большой правый поворот
                }
            }
            else if (b_factor < -1)//иначе если правый дочерний узел выше левого больше чем на 1
            {
                if (balance_factor(current.right) > 0)//если у этого дочернего узла правый дочерний узел выше левого
                {
                    current = BigRotateLeft(current);//то соверщаем большой левый поворот
                }
                else//иначе
                {
                    current = RotateLeft(current);//совершаем левый поворот
                }
            }
            return current;//возвращаем новый корень поддерева
        }

        public void Delete(int target)//удаление узла со значением target
        {
            root = Delete(root, target);//передаем в рекурсию в качестве текущего узла корень поддерева
        }

        private Node Delete(Node current, int target)//рекурсивный поиск узла, который нужно удалить 
        {
            Node parent;//Вспомогательный узел
            if (current == null)//если не нашли узел, который нужно удалить
            { return null; }//то и удалять нечего, возвращаем null

            else//иначе Ищем узел,который нужно удалть
            {                
                if (target < current.key)//если искомый узел меньше обрабатываемого
                {
                    current.left = Delete(current.left, target);//запускаем рекурсию влево(спускаемся от корня влево вниз                    
                    current = balance_tree(current);//балансировка
                }                
                else if (target > current.key)//иначе если значение искомого больше обрабатываемого
                {
                    current.right = Delete(current.right, target);//запускаем рекурсию вправо(спускаемся от корня вправо вниз)
                    current = balance_tree(current);//балансировка
                }
                
                else//Когда нашли узел, который нужно удалить
                {
                    //(! при удалении, вместо удаленного узла, вместо него будет минимальный элемент правого поддерева)
                    if (current.right != null)//если у обрабатываемого узла есть правый потомок
                    {                        
                        parent = current.right;//запоминаем правого потомка
                        while (parent.left != null)//спускаемся влево и вниз пока не упремся в null
                        {
                            parent = parent.left;//находим самый левый нижний в поддереве(минимальный)
                        }
                        current.key = parent.key;//значение текущего теперь значение минимльного узла
                        current.right = Delete(current.right, parent.key);//рекурсия вправо
                        current = balance_tree(current);//балансировка
                    }
                   
                    else //иначе если правого потомкоа нет
                    {                           
                        return current.left;//то это лист и его можно спокойно удалить без поворотов
                    }
                }
            }
            return current;//произошли все повороты, текущий узел стал листом и его можно удалить
        }

        int delta;
        int size_w1;

        public void Draw(Graphics _gr, int size_w, int size_h)
        {
            _gr.FillRectangle(Brushes.White, 0, 0, size_w, size_h);
            delta = size_h / (Deep + 2);
            size_w1 = size_w;
            DrawNode(_gr, ref root, size_w / 2, size_h, 0);
        }

        public void Clear()
        {
            root = null;
        }

        void DrawNode(Graphics _gr, ref Node _root, int size_w, int size_h, int level, int x0 = -1, int y0 = -1)
        {
            if (_root == null) return;
            Font f = new Font("Arial", 10);
            int y = (level + 1) * delta;
            int x = size_w;
            if (level > 0)
            {
                _gr.DrawLine(Pens.Black, x - 5, y - 5, x0, y0 + 20);

            }
            x0 = x;
            y0 = y;
            _gr.DrawEllipse(Pens.Black, x - 10, y - 10, 30, 30);
            _gr.DrawString(_root.key.ToString(), f, Brushes.Black, x, y);

            int delta_x = size_w1 / (Convert.ToInt32(Math.Pow(2, level + 2)) + 1);
            if (_root.left != null)
            {
                int x1 = x - delta_x;
                DrawNode(_gr, ref _root.left, x1, size_h, level + 1, x0, y0);
            }
            if (_root.right != null)
            {
                int x1 = x + delta_x;
                DrawNode(_gr, ref _root.right, x1, size_h, level + 1, x0, y0);
            }
        }

        int DeepTree(ref Node _root)
        {
            int h1 = 0, h2 = 0;
            if (_root == null) return 0;
            if (_root.left != null) { h1 = DeepTree(ref _root.left); }
            if (_root.right != null) { h2 = DeepTree(ref _root.right); }
            return h1 > h2 ? h1 + 1 : h2 + 1;
        }

        public int Deep
        {
            get
            {
                return DeepTree(ref root) - 1;
            }
        }
    }    
}


