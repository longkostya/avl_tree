﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace MyBinaryTree
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox1.Focus();
        }

        AVLtree Tree = new AVLtree();

        private void button1_Click(object sender, EventArgs e)
        {
            
            Tree.insert(Convert.ToInt32(textBox1.Text));   
            label2.Text = "Глубина: " + Tree.Deep.ToString();            
            textBox1.Clear();
            Tree.Draw(gr, panel1.Width, panel1.Height);
            textBox1.Focus();
        }

        Graphics gr;

        private void Form1_Load(object sender, EventArgs e)
        {
            
            label2.Text = "Глубина: " + Tree.Deep.ToString();
            gr = panel1.CreateGraphics();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Tree.Clear();            
            label2.Text = "Глубина: " + Tree.Deep.ToString();
            textBox3.Clear();            
            textBox1.Clear();
            Tree.Draw(gr, panel1.Width, panel1.Height);
            textBox1.Focus();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           int tmp = Convert.ToInt32(textBox3.Text);
            Tree.Delete(tmp);
            Tree.Draw(gr, panel1.Width, panel1.Height);
            textBox3.Clear();
            textBox3.Focus();
        }
    }
}
